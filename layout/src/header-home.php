<!DOCTYPE html>
<html lang="pt-br">
  <head>
    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-T4X52L');</script>
    <!-- End Google Tag Manager -->

    <!-- Meta tags Obrigatórias -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link rel="stylesheet" href="css/style.css">
    <link href="https://fonts.googleapis.com/css?family=Rajdhani:300,400,500,600,700" rel="stylesheet">

    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.1/css/all.css" integrity="sha384-5sAR7xN1Nv6T6+dT2mhtzEpVJvfS3NScPQTrOxhwjIuvcA67KV2R5Jz6kr4abQsz" crossorigin="anonymous">

    <title>JN2 e-commerce expert</title>
  </head>
  <body>

  <!-- Google Tag Manager (noscript) -->
  <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-T4X52L"
  height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
  <!-- End Google Tag Manager (noscript) -->

  <!-- Modal - Fale com um Expert -->
  <?php include_once 'modal-fale-com-expert.php'; ?>

  <div class="wrapper">

  <!-- Header -->
  <header>
    <div class="container">
      <div class="flex-row">
        <nav class="navbar navbar-expand-lg navbar-light justify-content-between">

          <!-- Marca -->
          <div class="d-flex order-1 order-lg-1">
            <a class="navbar-brand" href="#">
              <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 86.4 91" height="91" width="87">
                <g fill="#FFF">
                  <path d="M62 20.7h7.5c2.4 0 4.7 0 6.9 1 1.9.9 3.5 2.5 4.4 4.5.9 2.2.8 4.4.4 6.7-.9 5.1-5.1 8.4-8.7 11.8-3.5 3.3-7.1 6.7-10.7 10-1.2 1.1-2.4 2.3-3.6 3.4-.5.4-.9.9-1.1 1.5-.5 1.8.9 3.8 2.7 3.8h22c.8 0 1.7.1 2.5 0 1.5-.2 2.5-1.8 1.9-3.2-.4-1-1.4-1.4-2.4-1.5-6.4-.2-12.9 0-19.3 0l.7-.7c3.3-3.1 6.7-6.3 10-9.4 3.3-3.1 7-6.1 9-10.2 1.6-3.1 1.8-6.3 1.8-9.7 0-3-.9-6-3-8.3-3.7-4.2-9.2-4.4-14.4-4.4h-9.4c-1.4.1-2.8 1.4-2.4 2.9.8 2.2 3.5 1.8 5.2 1.8zM9.2 2.5c0 1.4-.1 2.9 0 4.2.3 3.1 4.8 2.7 5-.3.1-1.4.1-2.9 0-4.3-.4-3-5-2.8-5 .4zM14.2 37V22.3v-3.4c0-.9-.3-1.7-1-2.2-1.7-1.2-3.9 0-3.9 2v38.9c0 2.8-.3 5.6-1.3 8.3-1 2.6-2.7 4.9-4.3 7.2-.8 1.1-1.5 2.2-2.3 3.2C.7 77.1 0 77.9 0 79c0 1.9 1.9 2.9 3.6 2.2.8-.3 1.3-1.3 1.7-2 3.5-5 7.5-9.9 8.4-16.1.5-3.4.4-7 .4-10.5.1-5.1.1-10.3.1-15.6zM21.2 51.6v8.7c0 1.3.4 2.8 1.9 3.1 2 .5 3.1-1.3 3.1-3.1V38.5 25.8v-4c0-.2-.1-1.2 0-1.3.2-.2 1.4 0 1.7 0h9c6.1.1 10.1 5.1 10.1 11v29.4c0 1 .4 1.8 1.3 2.3 1.5.8 3.4-.2 3.6-1.8.1-.7 0-1.6 0-2.3V54 31.6c0-5.7-2.5-11.3-7.8-14-3.8-2-8.1-1.7-12.3-1.7h-6.1c-2.2 0-4.3.9-4.7 3.4-.3 1.9-.1 4-.1 6 .3 8.7.3 17.5.3 26.3zM23 74.5c1-.1 2-.1 2.8-.7.7-.5 1-1.5.5-2.3-.5-.8-1.7-.9-2.5-.7-1 .3-1.7 1-2.2 2-.8 1.9-.6 4.5 2 4.5.4 0 1.8-.1 2-.5.1-.2 0-.8 0-1-.8.4-2.1.9-2.8.1-.2-.2-.4-.8-.3-1.1.2-.4.1-.2.5-.3zm1.5-2.8c1 0 .8 1 .2 1.4-.6.4-1.3.4-2 .4.3-.8.9-1.8 1.8-1.8zM36.1 72.1c.1-.2.3-.6.3-.9 0-.3.1 0-.1-.3-.1-.2-.6-.2-.8-.2-1.2-.2-2.4.2-3.2 1.1-1.4 1.8-1.5 5.4 1.4 5.4.5 0 1.5-.1 1.8-.5.1-.2 0-.8 0-1-.6.3-1.5.7-2.2.3-.7-.4-.7-1.3-.5-2 .3-1.7 1.6-2.6 3.3-1.9zM39.3 70.8c-1.6.5-2.3 2.3-2.4 3.8 0 1.8 1.4 3 3.2 2.5 1.8-.4 2.5-2.3 2.5-4 0-1.7-1.6-2.8-3.3-2.3zm1.9 2.3c0 .9-.2 2.3-1.1 2.8-1 .6-1.8-.1-1.8-1.1 0-.9.3-2.3 1.1-2.8.9-.6 1.8.1 1.8 1.1zM46.7 72c1.6-.9 1.2 1.3 1 2.2l-.6 3c.3 0 1.2.2 1.4 0 .2-.1.2-1.1.3-1.3.3-1.3.4-3 1.5-3.8.8-.6 1.5-.1 1.4.8 0 .4-.2.9-.3 1.3l-.6 3c.3 0 .8.1 1.1 0 .4-.1.1.1.3-.3.4-.7.4-1.9.6-2.8.2-.9.5-2 0-2.8-.4-.6-1.2-.7-1.9-.5-.7.2-1.1.8-1.6 1.3-.1-.8-.6-1.4-1.5-1.4-1.1 0-1.4.8-2.1 1.3 0-.4.1-.8.1-1.2-.2 0-.9-.1-1 0-.1.1 0 .2-.1.3-.3.6-.3 1.5-.4 2.1-.3 1.3-.6 2.6-.8 4 .3 0 1.2.1 1.4 0 .2-.2.2-1 .3-1.3.1-1.3.2-3.2 1.5-3.9zM61.3 70.8c-.7.2-1.1.8-1.6 1.3-.1-.8-.6-1.4-1.5-1.4-1 0-1.4.8-2.1 1.3 0-.4.1-.8.1-1.2-.6 0-.9-.2-1.1.3-.3.6-.3 1.5-.4 2.1-.3 1.3-.6 2.6-.8 4 .3 0 1.2.1 1.4 0 .2-.2.2-1 .3-1.3.3-1.3.4-3.2 1.7-3.9 1.6-.9 1.2 1.3 1 2.2l-.6 3c.3 0 1.2.2 1.4 0 .2-.1.2-1.1.3-1.3.3-1.3.4-3 1.5-3.8.8-.6 1.5-.1 1.4.8 0 .4-.2.9-.3 1.3l-.6 3c.3 0 .8.1 1.1 0 .4-.1.1.1.3-.3.4-.7.4-1.9.6-2.8.2-.9.5-2 0-2.8-.6-.6-1.5-.7-2.1-.5zM67.2 70.8c-1 .3-1.7 1-2.2 2-.8 1.9-.6 4.5 2 4.5.4 0 1.8-.1 2-.5.1-.2 0-.8 0-1-.8.4-2.1.9-2.8.1-.2-.2-.4-.8-.3-1.1.1-.3 0-.2.4-.3 1-.1 2-.1 2.8-.7.7-.5 1-1.5.5-2.3-.4-.8-1.6-.9-2.4-.7zm.9 2.4c-.6.4-1.3.4-2 .4.2-.8.8-1.8 1.7-1.8 1-.1.9 1 .3 1.4zM72.8 72c0-.4.1-.8.1-1.2-.2 0-.9-.1-1 0-.1.1 0 .2-.1.3-.3.6-.3 1.5-.4 2.1-.3 1.3-.6 2.6-.8 4 .5 0 1.1.1 1.4-.2.3-.4.4-1.7.5-2.3.2-1 .5-2.3 1.6-2.7.3-.1.6 0 .8-.1h.3s0-.1.1-.2c.2-.2.2-.7.2-1-.2 0-.4-.1-.6-.1-1.1.1-1.4.9-2.1 1.4z"/>
                  <path d="M75.1 71.9c0 .1.1 0 0 0zM79.7 70.8c-1.2-.2-2.4.2-3.2 1.1-1.4 1.8-1.5 5.4 1.4 5.4.5 0 1.5-.1 1.8-.5.1-.2 0-.8 0-1-.6.3-1.5.7-2.2.3-.7-.4-.7-1.3-.5-2 .3-1.8 1.5-2.7 3.2-2 .1-.2.3-.6.3-.9 0-.3.1 0-.1-.3 0-.1-.5-.1-.7-.1zM86.2 71.5c-.5-.8-1.7-.9-2.5-.7-1 .3-1.7 1-2.2 2-.8 1.9-.6 4.5 2 4.5.4 0 1.8-.1 2-.5.1-.2 0-.8 0-1-.8.4-2.1.9-2.8.1-.2-.2-.4-.8-.3-1.1.1-.3 0-.2.4-.3 1-.1 2-.1 2.8-.7.7-.5 1-1.5.6-2.3zm-1.6 1.7c-.6.4-1.3.4-2 .4.2-.8.8-1.8 1.7-1.8 1-.1.9 1 .3 1.4zM25.3 82.4c-.5-.8-1.7-.9-2.5-.7-1 .3-1.7 1-2.2 2-.8 1.9-.6 4.5 2 4.5.4 0 1.8-.1 2-.5.1-.2 0-.8 0-1-.8.4-2.1.9-2.8.1-.2-.2-.4-.8-.3-1.1.1-.3 0-.2.4-.3 1-.1 2-.1 2.8-.7.7-.5 1-1.5.6-2.3zM23.8 84c-.5.4-1.4.4-2.1.4.2-.8.8-1.8 1.7-1.8.9.1.9 1 .4 1.4zM29.6 84.9c.1-.5 1-1.2 1.4-1.6.4-.5.8-1 1.3-1.5-.7 0-1.3-.2-1.8.3-.6.5-1 1.3-1.5 1.9-.2-.6-.4-1.5-.7-1.9-.3-.4-1-.2-1.5-.2.3.6.5 1.3.8 1.9.2.4.5.9.4 1.3-.2.6-1 1.3-1.4 1.7l-1.2 1.5c.7 0 1.3.2 1.8-.3.6-.6 1-1.3 1.5-1.9.2.6.4 1.5.8 2 .2.3 0 .2.4.3.3.1.8 0 1.2 0-.3-.6-.6-1.2-.8-1.9-.3-.6-.8-1.3-.7-1.6zM36.5 81.6c-1-.1-1.5.6-2.2 1.2 0-.4.1-.7.1-1.1-1 0-1-.2-1.2.7-.2 1-.4 2.1-.7 3.1-.4 1.8-.8 3.6-1.1 5.4.3 0 1 .1 1.2 0 .2-.1.2-.3.2-.6.2-1 .5-2.1.6-3.1.5.5.8 1 1.6 1 2.2 0 3.2-2.9 3-4.7 0-.9-.5-1.8-1.5-1.9zM35 87.1c-2.1.2-1-4.4.8-4.4 1.9 0 .7 4.2-.8 4.4zM41.8 81.7c-1 .3-1.7 1-2.2 2-.9 1.9-.6 4.5 2 4.5.4 0 1.8-.1 2-.5.1-.2 0-.8 0-1-.8.4-2.1.9-2.8.1-.2-.2-.4-.8-.3-1.1.1-.3 0-.2.4-.3 1-.1 2-.1 2.8-.7.7-.5 1-1.5.5-2.3-.4-.8-1.6-.9-2.4-.7zm.9 2.4c-.6.4-1.3.4-2 .4.2-.8.8-1.8 1.7-1.8 1 0 .9 1 .3 1.4zM47.4 82.9c0-.4.1-.8.1-1.2-.2 0-.9-.1-1 0-.1.1 0 .2-.1.3-.3.6-.3 1.5-.4 2.1-.3 1.3-.6 2.6-.8 4 .5 0 1.1.1 1.4-.2.3-.4.4-1.7.5-2.3.2-1 .5-2.3 1.6-2.7.3-.1.6 0 .8-.1h.3s0-.1.1-.2c.2-.2.2-.7.2-1-.2 0-.4-.1-.6-.1-1.1.1-1.4.9-2.1 1.4zM49.7 82.9s.1 0 0 0zM53 80.3c-.3 0-.7-.1-.9.1-.3.2-.4.8-.6 1.1-.2.3-.6.3-.9.5-.3.2-.3.3-.4.8h.9c-.2 1.1-.5 2.2-.7 3.3-.2 1.1 0 2 1.2 2.2.4 0 1.3 0 1.5-.3.1-.2 0-.8 0-1-.3.1-.6.2-.9.2-1.1 0-.1-3.3.1-3.9.2-.6.4-.4 1.1-.4.2 0 .5.1.6 0 .1-.1.2-.8.2-1h-1.6c.2-.6.3-1.1.4-1.6zM27.2 73.6s-.1.8 0 .8c.2.2 1.8 0 2.1 0 .1 0 .9.1 1 0 .1-.1 0-.6 0-.8H28c-.2 0-.8-.1-.8 0z"/>
                </g>
              </svg>
            </a>
          </div>

          <!-- Navbar -->
          <div class="d-flex order-4 order-lg-2">
            <div id="navegacao" class="navbar-collapse collapse">
              <ul class="navbar-nav navbar-dark menu">
                <li class="nav-item dropdown menu-item-has-children">
                  <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Plataforma JN2</a>
                  <div class="dropdown-menu">
                    <a class="dropdown-item" href="#recursos">Recursos</a>
                    <a class="dropdown-item" href="#planos">Planos</a>
                  </div>
                </li>
                <li class="nav-item"><a class="nav-link" href="grandes-empresas">Outsourcing</a></li>
                <li class="nav-item dropdown menu-item-has-children">
                  <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Conteúdos</a>
                  <div class="dropdown-menu">
                    <a class="dropdown-item" href="blog">Blog</a>
                    <a class="dropdown-item" href="#newsletter">Newsletter</a>
                    <a class="dropdown-item" href="materiais-gratuitos">E-books</a>
                  </div>
                </li>
                <li class="nav-item dropdown menu-item-has-children">
                  <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Sobre</a>
                  <div class="dropdown-menu">
                    <a class="dropdown-item" href="quem-somos">Quem somos</a>
                    <a class="dropdown-item" href="trabalhe-conosco">Trabalhe conosco</a>
                    <a class="dropdown-item" href="seja-um-parceiro-da-jn2">Seja um parceiro</a>
                  </div>
                </li>
                <li class="nav-item"><a class="nav-link" href="https://jn2.zendesk.com/hc/">Área do Cliente</a></li>
              </ul>
            </div>
          </div>

          <!-- Conversão -->
          <div class="d-flex order-2 order-lg-3">
            <div class="btn-group conversao-topo">
              <ul>
                <li class="nav-item dropdown menu-item-has-children">           
                  <a id="BtnLigueTopoHome" class="btn btn-outline-light d-none d-md-block" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Ligue ou mande um Whatsapp</a>
                  <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                    <a class="dropdown-item" href="tel:08008871640"><i class="fas fa-phone"></i> Ligue 0800 887 1640</a>
                    <a class="dropdown-item" href="https://api.whatsapp.com/send?l=pt_BR&phone=55031991574383" target="_blank"><i class="fab fa-whatsapp"></i> Mande um Whatsapp</a>
                  </div>
                </li>
                <li class="nav-item"><a id="BtnTesteTopoHome" class="btn btn-orange-hard" href="comece-seu-teste-gratis" role="button">Faça um teste grátis</a></li>
              </ul>
            </div>
          </div>

          <!-- Botão Mobile -->
          <div class="d-flex order-3 order-lg-4">
            <button id="btn-hamburger-mobile" class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navegacao" aria-controls="conteudoNavbarSuportado" aria-expanded="false" aria-label="Alterna navegação">
              <span class="navbar-toggler-icon"></span>
            </button>
          </div>
        </nav>
      </div>
    </div>
  </header>

  <div class="background">
  </div>

  <!-- Banner principal -->
  <div class="jumbotron">
    <div class="container">
      <div class="row">
        <div class="col-12 col-md-9">
          <h1><span>Crie sua Loja Virtual e venda mais</span> com uma plataforma que se adapta a todos os segmentos de mercado e cresce com o seu negócio.</h1>
          <p>Independente de qual seja o volume de vendas e o modelo de negócios da sua empresa, a plataforma JN2 oferece todos os recursos necessários para você vender seus produtos on-line.</p>
        </div>
      </div>

      <!-- Conversão -->
      <div class="row conversao">
        <div class="col-12 col-md-9 d-md-flex">
          <div class="d-flex d-md-block justify-content-center">
            <a id="BtnFaleTopoHome" class="btn btn-orange-hard" href="#" role="button" data-toggle="modal" data-target="#ModalExpert">Fale com um expert</a>
          </div>

          <div class="d-flex d-md-block justify-content-center">
            <p class="ou">OU</p>
          </div>

          <div class="d-md-block justify-content-center">
            <a id="BtnTesteBannerHome" class="btn btn-orange-hard" href="comece-seu-teste-gratis" role="button">Faça um teste grátis</a>
            <p class="seguranca"><small>Teste por 15 dias, sem compromissos.<br />
            Não precisa de cartão de crédito.</small></p>
          </div>

          <div class="d-flex d-md-block justify-content-center">
            <div class="selo">
              <span class="font-weight-bold numero">12</span><br /> 
              <span class="font-weight-bold anos">anos</span><br /> 
              de experiência<br /> no mercado
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>