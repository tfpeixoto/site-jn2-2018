<?php
session_start();

    $_SESSION['nome'] = !empty($_POST['nome']) ? $_POST['nome'] : '';
    $_SESSION['email'] = !empty($_POST['email']) ? $_POST['email'] : '';
    $_SESSION['ddd'] = !empty($_POST['ddd']) ? $_POST['ddd'] : '';
    $_SESSION['telefone'] = !empty($_POST['telefone']) ? $_POST['telefone'] : '';
    $_SESSION['empresa'] = !empty($_POST['empresa']) ? $_POST['empresa'] : '';
?>

<?php require_once("header-inner.php"); ?>

  <div class="dados-loja">
    <div class="container">
      <div class="row justify-content-center">
        <div class="col col-sm-8 my-mdb">
          <h2>Comece seu Teste Grátis</h2>
        </div>
      </div>

      <div class="row justify-content-center orientacao">
        <div class="col col-sm-4 concluido">
          <div><a href="comece-seu-teste-gratis">Preencha seus<br /> Dados</a></div>
          <div class="item mx-auto"><a href="comece-seu-teste-gratis">1</a></div>
        </div>

        <div class="col col-sm-4 ativo">
          <div>Escolha o tema<br /> da sua loja</div>
          <div class="item mx-auto">2</div>
        </div>
      </div>

      <div class="row justify-content-center">
        <div class="col-5 col-sm-4 progresso-concluido">
        </div>
      </div>
    </div>

    <div class="container">
      <div class="row justify-content-center mt-mdb">
        <div class="col-9 col-sm-6">
          <h3>
            <div class="numero">2</div>
            <div class="titulo">Escolha um tema para sua loja</div>
          </h3>
        </div>
      </div>
    </div>
  
    <div class="container">
      <div class="flex-row d-flex justify-content-center">
        <div class="col-10 col-sm-8">
          <div id="selecionar-segmento" class="row form-group">
            <label for="segmento" class="col-12 col-sm-7">Você está vendo temas para lojas de:</label>
            <select id="segmento" class="form-control col-12 col-sm-5">
              <option value="temas-moda">Moda</option>
              <option value="temas-beleza">Beleza</option>
              <option value="temas-deco">Móveis e Decoração</option>
              <option value="temas-eletro">Eletrônicos</option>
              <option value="temas-esporte">Esportes</option>
              <option value="temas-games">Games</option>
            </select>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="temas">
    <div class="container">
      <div class="flex-row justify-content-center">
        <!-- MODA -->
        <div id="temas-moda" class="row temas-segmento">
          <div class="col-12 col-sm-3" role="tablist">
            <div class="nav flex-column nav-pills" id="pills-moda" role="tablist" aria-orientation="vertical">
              <a id="AGILE2_MODA_CLEAN" class="nav-link active tema-selecionado" data-toggle="pill" href="#moda-clean" role="tab" aria-controls="moda-clean" aria-selected="true">
                <img src="images/temas/thumb-moda-clean.jpg" alt=""> 
              </a>
              <p>Moda Clean</p>

              <a id="AGILE2_MODA_FLAT" class="nav-link" data-toggle="pill" href="#moda-flat" role="tab" aria-controls="moda-flat" aria-selected="false">
                <img src="images/temas/thumb-moda-flat.jpg" alt="">
              </a>
              <p>Moda Flat</p>

              <a id="AGILE2_MODA_CROSS" class="nav-link" data-toggle="pill" href="#moda-cross" role="tab" aria-controls="moda-cross" aria-selected="false">
                <img src="images/temas/thumb-moda-cross.jpg" alt="">
              </a>
              <p>Moda Cross</p>
            </div>
          </div>

          <div class="d-none d-sm-flex justify-content-right align-items-center col-sm-9">
            <div class="tab-content" id="ConteudoModa">
              <div class="tab-pane fade active show" id="moda-clean" role="tabpanel" aria-labelledby="moda-clean">
                <img src="images/temas/tema-moda-clean.jpg" alt="Tema moda clean">
              </div>
              <div class="tab-pane fade" id="moda-flat" role="tabpanel" aria-labelledby="moda-flat">
                <img src="images/temas/tema-moda-flat.jpg" alt="Tema moda flat">
              </div>
              <div class="tab-pane fade" id="moda-cross" role="tabpanel" aria-labelledby="moda-cross">
                <img src="images/temas/tema-moda-cross.jpg" alt="Tema moda cross">
              </div>
            </div>
          </div>
        </div>

        <!-- BELEZA -->
        <div id="temas-beleza" class="row temas-segmento" style="display: none">
          <div class="col-12 col-sm-3" role="tablist">
            <div class="nav flex-column nav-pills" id="pills-beleza" role="tablist" aria-orientation="vertical">
              <a id="AGILE2_BELEZA_CLEAN" class="nav-link" data-toggle="pill" href="#beleza-clean" role="tab" aria-controls="beleza-clean" aria-selected="false">
                <img src="images/temas/thumb-beleza-clean.jpg" alt="">                
              </a>
              <p>Beleza Clean</p>

              <a id="AGILE2_BELEZA_FLAT" class="nav-link" data-toggle="pill" href="#beleza-flat" role="tab" aria-controls="beleza-flat" aria-selected="false">
                <img src="images/temas/thumb-beleza-flat.jpg" alt="">                
              </a>
              <p>Beleza Flat</p>

              <a id="AGILE2_BELEZA_CROSS" class="nav-link" data-toggle="pill" href="#beleza-cross" role="tab" aria-controls="beleza-cross" aria-selected="false">
                <img src="images/temas/thumb-beleza-cross.jpg" alt="">                
              </a>
              <p>Beleza Cross</p>
            </div>
          </div>

          <div class="d-none d-sm-flex justify-content-right align-items-center col-sm-9">
            <div class="tab-content" id="ConteudoBeleza">
              <div class="tab-pane" id="beleza-clean" role="tabpanel" aria-labelledby="beleza-clean">
                <img src="images/temas/tema-beauty-clean.jpg" alt="Tema beauty clean">
              </div>
              <div class="tab-pane fade" id="beleza-flat" role="tabpanel" aria-labelledby="beleza-flat">
                <img src="images/temas/tema-beauty-flat.jpg" alt="Tema beauty flat">
              </div>
              <div class="tab-pane fade" id="beleza-cross" role="tabpanel" aria-labelledby="beleza-cross">
                <img src="images/temas/tema-beauty-cross.jpg" alt="Tema beauty cross">
              </div>
            </div>
          </div>
        </div>

        <!-- DECORAÇÃO -->
        <div id="temas-deco" class="row temas-segmento" style="display: none">
          <div class="col-12 col-sm-3" role="tablist">
            <div class="nav flex-column nav-pills" id="pills-deco" role="tablist" aria-orientation="vertical">
              <a id="AGILE2_DECO_CLEAN" class="nav-link" data-toggle="pill" href="#deco-clean" role="tab" aria-controls="deco-clean" aria-selected="false">
                <img src="images/temas/thumb-deco-clean.jpg" alt="">                
              </a>
              <p>Deco Clean</p>

              <a id="AGILE2_DECO_FLAT" class="nav-link" data-toggle="pill" href="#deco-flat" role="tab" aria-controls="deco-flat" aria-selected="false">
                <img src="images/temas/thumb-deco-flat.jpg" alt="">                
              </a>
              <p>Deco Flat</p>

              <a id="AGILE2_DECO_CROSS" class="nav-link" data-toggle="pill" href="#deco-cross" role="tab" aria-controls="deco-cross" aria-selected="false">
                <img src="images/temas/thumb-deco-cross.jpg" alt="">                
              </a>
              <p>Deco Cross</p>
            </div>
          </div>

          <div class="d-none d-sm-flex justify-content-right align-items-center col-sm-9">
            <div class="tab-content" id="Conteudodeco">
              <div class="tab-pane" id="deco-clean" role="tabpanel" aria-labelledby="deco-clean">
                <img src="images/temas/tema-deco-clean.jpg" alt="Tema deco clean">
              </div>
              <div class="tab-pane fade" id="deco-flat" role="tabpanel" aria-labelledby="deco-flat">
                <img src="images/temas/tema-deco-flat.jpg" alt="Tema deco flat">
              </div>
              <div class="tab-pane fade" id="deco-cross" role="tabpanel" aria-labelledby="deco-cross">
                <img src="images/temas/tema-deco-cross.jpg" alt="Tema deco cross">
              </div>
            </div>
          </div>
        </div>

        <!-- ELETRO -->
        <div id="temas-eletro" class="row temas-segmento" style="display: none">
          <div class="col-12 col-sm-3" role="tablist">
            <div class="nav flex-column nav-pills" id="pills-eletro" role="tablist" aria-orientation="vertical">
              <a id="AGILE2_ELETRO_CLEAN" class="nav-link" data-toggle="pill" href="#eletro-clean" role="tab" aria-controls="eletro-clean" aria-selected="false">
                <img src="images/temas/thumb-eletro-clean.jpg" alt="">                
              </a>
              <p>Eletro Clean</p>

              <a id="AGILE2_ELETRO_FLAT" class="nav-link" data-toggle="pill" href="#eletro-flat" role="tab" aria-controls="eletro-flat" aria-selected="false">
                <img src="images/temas/thumb-eletro-flat.jpg" alt="">              
              </a>
              <p>Eletro Flat</p>

              <a id="AGILE2_ELETRO_CROSS" class="nav-link" data-toggle="pill" href="#eletro-cross" role="tab" aria-controls="eletro-cross" aria-selected="false">
                <img src="images/temas/thumb-eletro-cross.jpg" alt="">                
              </a>
              <p>Eletro Cross</p>
            </div>
          </div>

          <div class="d-none d-sm-flex justify-content-right align-items-center col-sm-9">
            <div class="tab-content" id="ConteudoEletro">
              <div class="tab-pane" id="eletro-clean" role="tabpanel" aria-labelledby="eletro-clean">
                <img src="images/temas/tema-eletro-clean.jpg" alt="Tema eletro clean">
              </div>
              <div class="tab-pane fade" id="eletro-flat" role="tabpanel" aria-labelledby="eletro-flat">
                <img src="images/temas/tema-eletro-flat.jpg" alt="Tema eletro flat">
              </div>
              <div class="tab-pane fade" id="eletro-cross" role="tabpanel" aria-labelledby="eletro-cross">
                <img src="images/temas/tema-eletro-cross.jpg" alt="Tema eletro cross">
              </div>
            </div>
          </div>
        </div>

        <!-- ESPORTES -->
        <div id="temas-esporte" class="row temas-segmento" style="display: none">
          <div class="col-12 col-sm-3" role="tablist">
            <div class="nav flex-column nav-pills" id="pills-esporte" role="tablist" aria-orientation="vertical">
              <a id="AGILE2_ESPORTE_CLEAN" class="nav-link" data-toggle="pill" href="#esporte-clean" role="tab" aria-controls="esporte-clean" aria-selected="false">
                <img src="images/temas/thumb-esporte-clean.jpg" alt="">                
              </a>
              <p>Esporte Clean</p>

              <a id="AGILE2_ESPORTE_FLAT" class="nav-link" data-toggle="pill" href="#esporte-flat" role="tab" aria-controls="esporte-flat" aria-selected="false">
                <img src="images/temas/thumb-esporte-flat.jpg" alt="">                
              </a>
              <p>Esporte Flat</p>

              <a id="AGILE2_ESPORTE_CROSS" class="nav-link" data-toggle="pill" href="#esporte-cross" role="tab" aria-controls="esporte-cross" aria-selected="false">
                <img src="images/temas/thumb-esporte-cross.jpg" alt="">                
              </a>
              <p>Esporte Cross</p>
            </div>
          </div>

          <div class="d-none d-sm-flex justify-content-right align-items-center col-sm-9">
            <div class="tab-content" id="ConteudoEsporte">
              <div class="tab-pane" id="esporte-clean" role="tabpanel" aria-labelledby="esporte-clean">
                <img src="images/temas/tema-esporte-clean.jpg" alt="Tema esporte clean">
              </div>
              <div class="tab-pane fade" id="esporte-flat" role="tabpanel" aria-labelledby="esporte-flat">
                <img src="images/temas/tema-esporte-flat.jpg" alt="Tema esporte flat">Flat</p>
              </div>
              <div class="tab-pane fade" id="esporte-cross" role="tabpanel" aria-labelledby="esporte-cross">
                <img src="images/temas/tema-esporte-cross.jpg" alt="Tema esporte cross">
              </div>
            </div>
          </div>
        </div>

        <!-- GAMES -->
        <div id="temas-games" class="row temas-segmento" style="display: none">
          <div class="col-12 col-sm-3" role="tablist">
            <div class="nav flex-column nav-pills" id="pills-games" role="tablist" aria-orientation="vertical">
              <a id="AGILE2_GAMES_CLEAN" class="nav-link" data-toggle="pill" href="#games-clean" role="tab" aria-controls="esporte-clean" aria-selected="false">
                <img src="images/temas/thumb-games-clean.jpg" alt="">                
              </a>
              <p>Games Clean</p>

              <a id="AGILE2_GAMES_FLAT" class="nav-link" data-toggle="pill" href="#games-flat" role="tab" aria-controls="games-flat" aria-selected="false">
                <img src="images/temas/thumb-games-flat.jpg" alt="">                
              </a>
              <p>Games Flat</p>

              <a id="AGILE2_GAMES_CROSS" class="nav-link" data-toggle="pill" href="#games-cross" role="tab" aria-controls="games-cross" aria-selected="false">
                <img src="images/temas/thumb-games-cross.jpg" alt="">                
              </a>
              <p>Games Cross</p>
            </div>
          </div>

          <div class="d-none d-sm-flex justify-content-right align-items-center col-sm-9">
            <div class="tab-content" id="ConteudoGames">
              <div class="tab-pane" id="games-clean" role="tabpanel" aria-labelledby="games-clean">
                <img src="images/temas/tema-games-clean.jpg" alt="Tema games clean">
              </div>
              <div class="tab-pane fade" id="games-flat" role="tabpanel" aria-labelledby="games-flat">
                <img src="images/temas/tema-games-flat.jpg" alt="Tema flat clean">
              </div>
              <div class="tab-pane fade" id="games-cross" role="tabpanel" aria-labelledby="games-cross">
                <img src="images/temas/tema-games-cross.jpg" alt="Tema cross clean">
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="fechamento-pedido">
    <div class="container">
      <div class="row justify-content-center">
        <div class="col-8 col-sm-3 finalizacao">
          <form id="formEscolhaTema">
            <input type="hidden" id="token_rdstation" name="token_rdstation" value="c31e6dfcbb2967fde2bbbbd749ba911e">
            <input type="hidden" id="identificador" name="identificador" value="trial-temas">
            <input type="hidden" name="nome" value="<?php echo $_POST['nome']?>" />
            <input type="hidden" name="email" value="<?php echo $_POST['email']?>" />
            <input type="hidden" name="telefone" value="<?php echo $_POST['ddd'] . $_POST['telefone']?>" />
            <input type="hidden" name="empresa" value="<?php echo $_POST['empresa']?>" />

            <input id="EscolhaDoTema" type="hidden" name="tema" value="AGILE2_DECO_CLEAN" />

            <?php if($_SERVER['HTTP_HOST'] == "localhost" || end(explode('.', $_SERVER['HTTP_HOST'])) == 'loc') :?>
                <div class="g-recaptcha" data-sitekey="6Lc7zncUAAAAABFNzfZAOyUfHA1ljt2apw-lv0td"></div>

            <?php else: ?>
                <!-- ADICIONAR DADOS DO RE-CAPCHA DE PRODUÇÃO -->
            <?php endif ?>
            <button type="submit" id="btnCriarLoja" class="btn btn-orange mx-auto" data-toggle="modal" data-target="#ModalLoading">Criar loja</button>
          </form>
          <p class="font-italic"><small>Você receberá um e-mail para confirmação de criação da sua loja virtual.</small></p>
        </div>
      </div>
    </div>
  </div>

  <div class="modal fade" id="ModalLoading" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-xl">
      <div class="modal-content">
        <img src="images/loading.gif" alt="Loading">
      </div>
    </div>
  </div>

<?php require_once("footer-inner.php"); ?>