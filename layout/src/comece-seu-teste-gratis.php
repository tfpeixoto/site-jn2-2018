  <?php require_once("header-inner.php"); ?>

  <div class="dados-loja">
    <div class="container">
      <div class="row justify-content-center">
        <div class="col col-sm-8 my-mdb">
          <h2>Comece seu Teste Grátis</h2>
        </div>
      </div>

      <div class="row justify-content-center orientacao">
        <div class="col col-sm-4 ativo">
          <div>Preencha<br /> seus Dados</div>
          <div class="item mx-auto">1</div>
        </div>

        <div class="col col-sm-4 inativo">
          <div>Escolha o tema<br /> da sua loja</div>
          <div class="item mx-auto">2</div>
        </div>
      </div>

      <div class="row justify-content-center">
        <div class="col-5 col-sm-4 progresso">
        </div>
      </div>
    </div>

    <div class="container">
      <div class="row d-flex justify-content-center mt-mdb mt-sm-db">
        <div class="col-8 col-sm-4">
          <h3>
            <div class="numero">1</div>
            <div class="titulo">Dados de acesso</div>
          </h3>
        </div>
      </div>
    </div>

    <div class="container">
      <div class="row justify-content-center">
        <div class="col col-sm-5 form-dados">

          <!-- Dados do cliente -->
          <form action="escolha-o-tema-da-sua-loja.php" method="post" id="formComeceTesteGratis">
            <div class="form-row">
              <input type="hidden" id="token_rdstation" name="token_rdstation" value="c31e6dfcbb2967fde2bbbbd749ba911e">
              <input type="hidden" id="identificador" name="identificador" value="trial-dados">

              <label for="inputNome">Nome (obrigatório):</label>
              <input name="nome" type="nome" class="form-control" id="inputNome" placeholder="Digite seu nome completo" required>

              <label for="inputEmail">E-mail (obrigatório):</label>
              <input name="email" type="email" class="form-control" id="inputEmail" placeholder="Digite seu e-mail" data-toggle="popover" data-trigger="hover" data-placement="right" data-content="Seu e-mail será usado para acessar sua loja" required>

              <label for="inputTelefone">Telefone (obrigatório):</label>
              <div class="form-row">
                <div class="col-4">
                  <input name="ddd" type="text" class="form-control" id="inputDDD" placeholder="DDD" required>
                </div>
                <div class="col-8">
                  <input name="telefone" type="text" class="form-control" id="inputPhone" placeholder="00000-0000" data-toggle="popover" data-trigger="hover" data-placement="right" data-content="Entramos em contato para te ajudar com a loja" required>
                </div>
              </div>

              <label for="inputEmpresa">Empresa (obrigatório):</label>
              <input name="empresa" type="text" class="form-control" id="inputEmpresa" placeholder="Digite o nome da sua empresa" data-toggle="popover" data-trigger="hover" data-placement="right" data-content="Você pode alterar 
                o nome depois" required>
            </div>

            <div class="form-row justify-content-center">
              <div class="form-group justify-content-center comecar">
                <button type="submit" class="btn btn-orange mx-auto">Continuar</button>
                <p class="font-italic"><small>para a criação da loja</small></p>
              </div>
            </div>
          </form>
          <!-- Fim do form -->

        </div>
      </div>
    </div>
  </div>

  <?php require_once("footer-inner.php"); ?>