var rdStationSubmit = false;

// Exibir descrição dos recursos
$(document).ready(function () {

    WebFontConfig = {
      google: {
        families: ['Rajdhani:100,300,400,500,700']
      },
      timeout: 2000
    };

    //Loading Google Web Fonts
    (function (d) {
      var wf = d.createElement('script'), s = d.scripts[0];
      wf.src = 'https://ajax.googleapis.com/ajax/libs/webfont/1.6.26/webfont.js';
      wf.async = true;
      s.parentNode.insertBefore(wf, s);
    })(document);

    // HEADER - Após rolagem habilita menu fixo
    $(window).scroll(function () {
        if ($(window).scrollTop() > $(window).height()) {
            $("header").addClass("fixed-top open");
        } else {
            $("header").removeClass("fixed-top open");
        }
    });

    $('a').click(function () {
        var idTema = $(this).attr('id');
        $('#EscolhaDoTema').val(idTema);
    });

    // RECURSOS - Exibir texto auxiliar
    $('#recursos .nav-link').click(function () {
        $(this).parent().find('p').hide();
        $(this).find('p').show();
    });

    // DUVIDAS - Sombra e cor verde
    $('button').click(function () {
        $('button').parents('.card').removeClass('duvida-ativa');
        $(this).parents('.card').toggleClass('duvida-ativa');
        $(this).toggleClass('show');
    });

    // PLANOS - Exibir mais informações dos planos
    $('.btn-vermais').click(function () {
        $('.recursos-planos').slideToggle();
        if ($('.btn-vermais').text() == "Ver mais ") {
            $('.btn-vermais').html('Ver menos <i class="fas fa-angle-up"></i>');
        } else {
            $('.btn-vermais').html('Ver mais <i class="fas fa-angle-down"></i>');
        }
    });

    // NEWSLETTER - Submissão da Newsletter
    $('#BtnNewsFooterHome').click(function(){
        // se o envio for OK exibe #RetornoNewsOK
        let formData = $('#formEmailNewsLetter').serializeArray();
        let respRDstation = RdIntegration.post(formData);
        $('#RetornoNewsOk').fadeIn(500).delay(5000).fadeOut(500);

        return false;

        // se o envio der erro exibe #RetornoNewsErro
        // $('#RetornoNewsErro').show(); 
    });

    $(function () {
      $('[data-toggle="popover"]').popover()
    })
    

    // TEMAS - Exibir temas do segmento
    $('#segmento').on('change', function () {
        $('.temas-segmento').hide();
        $('#' + this.value).show();
    });

    // SELECIONAR TEMAS - Ativar seleção do tema
    $('.temas-segmento a').click(function(){
      $('.temas-segmento a').removeClass('tema-selecionado');
      $(this).addClass('tema-selecionado');
    });

    // FALE EXPERT - Validação do form
    $('#formRdStation').validate({
        rules: {
            nome: {
                required: true,
                minlength: 3,
                character: true,
            },
            email: {
                required: true,
                email: true,
            },
            ddd: {
                required: true,
                number: true,
                minlength: 2,
                maxlength: 2,
            },
            telefone: {
                required: true,
                number: true,
                minlength: 8,
                maxlength: 9,
            },
            empresa: {
                required: true,
                minlength: 3,
                character: true,
            },
        },
        messages: {
            nome: {
                required: "Campo obrigatório.",
                minlength: "Seu nome deve conter pelo menos 3 caracteres.",
                character: "Seu nome não pode conter caracteres especiais.",
            },
            email: {
                required: "Campo obrigatório.",
                email: "Insira um e-mail válido.",
            },
            ddd: {
                required: "Campo obrigatório.",
                number: "Insira um DDD válido.",
                minlength: "O DDD precisa ter 2 números.",
                maxlength: "O DDD precisa ter 2 números.",
            },
            telefone: {
                required: "Campo obrigatório.",
                number: "Insira somente números",
                minlength: "O número do telefone deve conter no mínimo 8 caracteres.",
                maxlength: "O número do telefone deve conter até 9 caracteres.",
            },
            empresa: {
                required: "Campo obrigatório.",
                minlength: "O nome da sua empresa deve conter pelo menos 3 caracteres.",
                character: "O nome da sua empresa não pode conter caracteres especiais.",
            },
        },
    });

    // FALE EXPERT - Integração RD Station
    $('#formRdStation').submit(function (e) {
        e.preventDefault();
        if ( $('#formRdStation').valid() ){
            let $telefone = $(this).find('input[name="telefone"]');
            let $DDD = $(this).find('input[name="ddd"]');
            let telefoneSemDDD = $telefone.val();
            $telefone.val($DDD.val() + $telefone.val());
            $DDD.attr('disabled', 'disabled');

            let formData = $(this).serializeArray();
            let resp = RdIntegration.post(formData);
            $telefone.val(telefoneSemDDD);
            $DDD.removeAttr('disabled');
            $('#ExpertSeraAcionado').fadeIn(500).delay(5000).fadeOut(500);

            //setTimeout(function() {
            //    $('#ModalExpert').modal('hide');
            //}, 2000);
        }

        return false;
    });

    // CRIAR LOJA - Validação do form
    $('#formComeceTesteGratis').validate({
        rules: {
            nome: {
                required: true,
                minlength: 3,
                character: true,
            },
            email: {
                required: true,
                email: true,
            },
            ddd: {
                required: true,
                number: true,
                minlength: 2,
                maxlength: 2,
            },
            telefone: {
                required: true,
                number: true,
                minlength: 8,
                maxlength: 9,
            },
            empresa: {
                required: true,
                minlength: 3,
                character: true,
            },
        },
        messages: {
            nome: {
                required: "Campo obrigatório.",
                minlength: "Seu nome deve conter pelo menos 3 caracteres.",
                character: "Seu nome não pode conter caracteres especiais.",
            },
            email: {
                required: "Campo obrigatório.",
                email: "Insira um e-mail válido.",
            },
            ddd: {
                required: "Campo obrigatório.",
                number: "Insira um DDD válido.",
                minlength: "O DDD precisa ter 2 números.",
                maxlength: "O DDD precisa ter 2 números.",
            },
            telefone: {
                required: "Campo obrigatório.",
                number: "Insira somente números",
                minlength: "O número do telefone deve conter no mínimo 8 caracteres.",
                maxlength: "O número do telefone deve conter até 9 caracteres.",
            },
            empresa: {
                required: "Campo obrigatório.",
                minlength: "O nome da sua empresa deve conter pelo menos 3 caracteres.",
                character: "O nome da sua empresa não pode conter caracteres especiais.",
            },
        },
    });

    // CRIAR LOJA - Envio dos dados do primeiro passo
    $('#formComeceTesteGratis').submit(function (e) {
        if ( $('formComeceTesteGratis').valid()){
            if (!rdStationSubmit) {
                e.preventDefault();
                let $telefone = $(this).find('input[name="telefone"]');
                let $DDD = $(this).find('input[name="ddd"]');
                let telefoneSemDDD = $telefone.val();
                $telefone.val($DDD.val() + $telefone.val());
                $DDD.attr('disabled', 'disabled');

                let formData = $(this).serializeArray();
                let resp = RdIntegration.post(formData);
                $telefone.val(telefoneSemDDD);
                $DDD.removeAttr('disabled');
                rdStationSubmit = true;

                setTimeout(function () {
                    $('#formComeceTesteGratis').submit();
                }, 1000);
            }
            else {
               // console.log('rodando submit segunda vez');
            }
        }
    });

    // CRIAR LOJA - Faz o ajax de criar loja no evento de submit e depois redireciona de acordo com resultado
    $('#btnCriarLoja').click(function (e) {
        e.preventDefault();
        let formData = $('#formEscolhaTema').serializeArray();
        let respRDstation = RdIntegration.post(formData);

        grecaptcha.ready(function () {
            grecaptcha.execute('6LcsCHgUAAAAAHDVgbHAfq75OIZ5toCypspjksuF', {action: 'criar_loja'})
            .then(function (token) {
                // Verify the token on the server.
                $.ajax({
                    url: 'processamento.php', // incluir (baseUrl +) e retirar (.php)
                    type: 'post',
                    dataType: 'json',
                    data: {
                        'tema': $('#EscolhaDoTema').val(),
                        'token': token
                    },
                    beforeSend: function () {
                        // mostrar o loading
                    },
                    success: function (resp) {
                        if (resp && resp.success) {
                            window.location.href = 'sucesso.php'; // incluir (baseUrl +) e retirar (.php)
                        }
                        // em caso de erro
                        else {
                            window.location.href = 'comece-seu-teste-gratis.php'; // incluir (baseUrl +) e retirar (.php)
                        }
                    },
                    complete: function () {
                        // esconder loading
                    }
                });
            });
        });
    });

    // Inclui classe no Header quando clicado (menu mobile)
    $("#btn-hamburger-mobile").click(function(){
        $(this).toggleClass("open");
        $("header").toggleClass("open");
    });

    // Inclui classe no Header e calcula o tamanho (dropdown)
    $("header .menu .menu-item-has-children").mouseover(function(){
        $("header").addClass("open");
        $("header").height($(this).closest(".dropdown-menu").height());
    });

        // Inclui classe no Header e calcula o tamanho (botão contato)
        $("header .conversao-topo .menu-item-has-children").mouseover(function(){
            $("header").addClass("open");
            $("header").height($(this).closest(".dropdown-menu").height());
        });

        // Inclui classe no Header e calcula o tamanho (ponteiro do mouse)
        $("header .menu .menu-item-has-children").mouseenter(function(){
            $("header").addClass("open");
            $("header").height($(this).find(".dropdown-menu").height()+$(".dropdown-menu").height()+ 30);
        });

   /*$("header .menu .menu-item-has-children").mouseover(function(){
        $("header").height($(this).find(".dropdown-menu").height()+$(this).find(".dropdown-menu").offset().top+30);
    });*/

    // Remove classe do Header e calcula o tamanho (dropdown)
    $("header .menu .menu-item-has-children").mouseleave(function(){
        $("header").removeClass("open");
        $("header").css({height: "auto"});
    });

        // Remove classe do Header e calcula o tamanho (dropdown)
        $("header .conversao-topo .menu-item-has-children").mouseleave(function(){
            $("header").removeClass("open");
            $("header").css({height: "auto"});
        });

        // Remove classe do Header e calcula o tamanho (ponteiro do mouse)
        $("header .menu .menu-item-has-children").mouseleave(function(){
            $("header").removeClass("open").css({height: "auto"});
        });

    $(".mobile-menu .menu-item-has-children a").click(function(e){
        e.preventDefault();
        $(this).parent().toggleClass("open");
        return false;
    });
});

/*
// Menu
$(document).ready(function(){
  $(document).on("click", "#btn-menu", function(){
    $("menu").toggleClass("open");
    $("header").toggleClass("open");
    $(this).toggleClass("open");
  });

  // Btn nav mobile
  $(".nav-toggler").click(function(){
    $(this).toggleClass("open");
    $(".wrapper").toggleClass("open");
    $("#mobile-menu").toggleClass("open");
  });

  // Aplica classe no Header
  $(".dropdown-toggle").mouseover(function(){
    $("header").addClass("open");
    $("header").height($(this).closest("ul").height());
  });

  // Redimensiona background - Dropdown
  $(".dropdown-toggle").mouseover(function(){
    $("header").height($(this).find("ul").height()+$(this).find("ul").offset().top+30);
  });

  // Remove redimensionamento - Dropdown
  $(".dropdown-toggle").mouseleave(function(){
    $("header").removeClass("open");
    $("header").css({height: "auto"});
  });

  // Aplica background - Menu após rolagem
  $(".floating-header .menu .menu-item-has-children").mouseenter(function(){
    $(".floating-header").addClass("subopen");
    $(".floating-header").height($(this).find("ul").height()+$(".floating-header").height()+ 30);
  });

  // Remove background - Menu após rolagem
  $(".floating-header .menu .menu-item-has-children").mouseleave(function(){
    $(".floating-header").removeClass("subopen").css({height: "auto"});
  });


  $(".mobile-menu .menu-item-has-children a").click(function(e){
    e.preventDefault();
    $(this).parent().toggleClass("open");
    return false;
  });

}(jQuery));*/