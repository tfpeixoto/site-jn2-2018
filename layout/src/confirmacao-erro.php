<?php require_once("header-inner.php"); ?>

  <div class="cointainer">
    <div class="row justify-content-center my-db">
      <div class="col col-sm-6 mensagem-erro">
        <h2>Seu token não foi validado!</h2>

        <p>Tente novamente criar sua loja virtual</p>

        <a href="comece-seu-teste-gratis" class="btn btn-orange">Criar minha loja virtual</a>
      </div>
    </div>
  </div>

<?php require_once("footer-inner.php"); ?>