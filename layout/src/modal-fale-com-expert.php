<div class="modal fade" id="ModalExpert" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-xl">
        <div class="modal-content bg-transparent">
            <form id="formRdStation">
                <input type="hidden" id="token_rdstation" name="token_rdstation" value="c31e6dfcbb2967fde2bbbbd749ba911e">
                <input type="hidden" id="identificador" name="identificador" value="fale-com-um-expert">

                <div class="modal-header d-flex justify-content-center" id="titulo-formulario">
                    <div class="row justify-content-center">
                        <div class="col-sm-11">
                            <h5 class="modal-title">Entre em contato e descubra como podemos te ajudar a criar sua Loja Virtual</h5>

                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    </div>
                </div>

                <div class="modal-body">
                    <div class="form-row">
                        <div id="ExpertSeraAcionado" class="alert alert-success col-sm-11 offset-sm-1" role="alert" style="text-align:center; display:none">
                            Mensagem enviada com sucesso! Em breve um de nossos Experts entrará em contato.
                        </div>

                        <div class="form-group col-sm-6 offset-sm-1">
                            <label for="inputNome">Nome (obrigatório):</label>
                            <div class="form-row">
                                <input class="form-control col-sm-11" type="text" id="inputNome" name="nome" placeholder="Digite seu nome completo" required />
                            </div>

                            <label for="inputEmail">E-mail (obrigatório):</label>
                            <div class="form-row">
                                <input class="form-control col-sm-11" type="email" id="inputEmail" name="email" placeholder="Digite seu e-mail" required />
                            </div>

                            <label for="inputTelefone" class="d-block">Telefone (obrigatório):</label>
                            <div class="form-row">
                                <div class="col-4">
                                    <input class="form-control" type="text" id="inputDDD" placeholder="DDD" name="ddd" required />
                                </div>
                                <div class="col-7">
                                    <input class="form-control" type="text" id="inputPhone" name="telefone" placeholder="_____-____" required />
                                </div>
                            </div>
                        </div>

                        <div class="form-group col-sm-5">
                            <label for="inputEmpresa">Empresa</label>
                            <div class="form-row">
                                <input class="form-control" type="text" id="Empresa" name="empresa" placeholder="Digite o nome da sua empresa" />
                            </div>

                            <label for="textareaMensagem">Mensagem</label>
                            <div class="form-row">
                                <textarea class="form-control" id="textareaMensagem" name="mensagem" placeholder="Digite sua mensagem"></textarea>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="modal-footer d-flex justify-content-center">
                    <button id="SubmitFaleExpert" type="submit" class="btn btn-orange mx-auto">Enviar mensagem</button>
                </div>
            </form>
        </div>
    </div>
</div>
<script type ='text/javascript' src="https://d335luupugsy2.cloudfront.net/js/integration/stable/rd-js-integration.min.js" async></script>