  <?php require_once("header-home.php"); ?>

  <!-- Clientes -->
  <div class="clientes py-mdb py-sm-db">
    <div class="container">
      <div class="row justify-content-center">
        <div class="col-12 col-sm-10">
          <h2>Conte com uma solução que já atende <span>centenas de Lojas Virtuais</span></h2>
        </div>

        <ul class="col-12">
          <li class="d-inline"><img src="images/marca-smartfit.jpg" alt="SmartFit"></li>
          <li class="d-inline"><img src="images/marca-raia.jpg" alt="Droga Raia"></li>
          <li class="d-inline"><img src="images/marca-drogasil.jpg" alt="Drogasil"></li>
          <li class="d-inline"><img src="images/marca-jolivi.jpg" alt="Jolivi"></li>
          <li class="d-inline"><img src="images/marca-sotreq.jpg" alt="Sotreq"></li>
          <li class="d-inline"><img src="images/marca-tropeira.jpg" alt="Tropeira"></li>
        </ul>
      </div>
    </div>
  </div>

  <!-- Situação do cliente -->
  <div class="situacao-cliente py-mdb py-sm-db">
    <div class="container">
      <div class="row justify-content-center">
        <div class="col col-lg-8">
          <h2>Qual é o cenário que melhor define sua situação atual?</h2>
        </div>

        <div class="col-12 col-lg-8">
          <ul class="nav nav-pills mb-3 d-flex justify-content-center" id="pills-tab" role="tablist">
            <li class="nav-item">
              <a id="BtnTenhoLojaSituacaoHome" class="btn btn-outline-purple-soft btn-lg btn-situacao active" id="pills-home-tab" data-toggle="pill" href="#pills-lojasim" role="tab" aria-controls="pills-lojasim" aria-selected="true">
                <i class="fas fa-circle"></i> Já tenho uma loja virtual</a>
            </li>
            <li class="nav-item">
              <a id="BtnNaoTenhoLojaSituacaoHome" class="btn btn-outline-purple-soft btn-situacao btn-lg" id="pills-profile-tab" data-toggle="pill" href="#pills-lojanao" role="tab" aria-controls="pills-lojanao" aria-selected="false">
                <i class="fas fa-circle"></i> Não tenho uma loja virtual</a>
            </li>
          </ul>

          <div class="tab-content" id="pills-tabContent">
            <div class="tab-pane fade show active" id="pills-lojasim" role="tabpanel" aria-labelledby="pills-lojasim">
              <h3>Sem a solução certa para o seu negócio, é provável que você enfrente um ou mais dos seguintes problemas:</h3>

              <ul class="list-unstyled">
                <li class="media">
                  <img src="images/icone-situacao.png" class="align-self-center"> 
                  Faltam integrações na sua ferramenta atual.
                </li>
                <li class="media">
                  <img src="images/icone-situacao.png" class="align-self-center">
                  Falta segurança à sua Loja Virtual, e isso atrapalha suas vendas.
                </li>
                <li class="media">
                  <img src="images/icone-situacao.png" class="align-self-center">
                  A solução atual não acompanha o crescimento da sua empresa.
                </li>
                <li class="media">
                  <img src="images/icone-situacao.png" class="align-self-center">
                  Sua Loja Virtual não recebe o suporte adequado ou precisa pagar muito por isso.
                </li>
                <li class="media">
                  <img src="images/icone-situacao.png" class="align-self-center">
                  Sua Loja fica fora do ar em alguns momentos devido à instabilidade da solução.
                </li>
              </ul>
            </div>

            <div class="tab-pane fade" id="pills-lojanao" role="tabpanel" aria-labelledby="pills-lojanao">
              <h3>Antes de construir sua Loja Virtual, é possível que você enfrente um ou mais dos seguintes desafios:</h3>

              <ul class="list-unstyled">
                <li class="media">
                  <img src="images/icone-situacao.png" class="align-self-center"> 
                  Dificuldade de implantar e manter uma Loja Virtual funcionando.
                </li>
                <li class="media">
                  <img src="images/icone-situacao.png" class="align-self-center">
                  Você ainda não sabe como começar a operar no meio digital.
                </li>
                <li class="media">
                  <img src="images/icone-situacao.png" class="align-self-center">
                  Soluções baratas não entregam valor para o seu negócio.
                </li>
                <li class="media">
                  <img src="images/icone-situacao.png" class="align-self-center">
                  A falta de conhecimento técnico te deixa com receio de investir em um e-commerce.
                </li>
                <li class="media">
                  <img src="images/icone-situacao.png" class="align-self-center">
                  Você não sabe se é seguro vender on-line.
                </li>
              </ul>
            </div>
          </div>
        </div>
      </div>

      <div class="row justify-content-center frase-convencimento">
        <div class="col col-lg-9">
          <p>Não importa qual o segmento do seu negócio, a plataforma de e-commerce JN2 vai te ajudar com todos estes desafios.</p>
        </div>
      </div>

      <!-- Conversão -->
      <div class="row conversao">
        <div class="col-12 col-md-12 d-sm-flex justify-content-center">
          <div class="d-flex d-md-block justify-content-center">
            <a id="BtnFaleSituacaoHome" class="btn btn-orange-hard" href="#" role="button" data-toggle="modal" data-target="#ModalExpert">Fale com um expert</a>
          </div>

          <div class="d-flex d-md-block justify-content-center">
            <p class="ou">OU</p>
          </div>

          <div class="d-md-block justify-content-center">
            <a id="BtnTesteSituacaoHome" class="btn btn-orange-hard" href="comece-seu-teste-gratis" role="button">Faça um teste grátis</a>
            <p class="seguranca"><small>Teste por 15 dias, sem compromissos.<br />
            Não precisa de cartão de crédito.</small></p>
          </div>
        </div>
      </div>
    </div>
  </div>

  <!-- Benefícios -->
  <div class="beneficios py-mdb py-sm-db">
    <div class="container">
      <div class="row"> 
        <div class="col-12 mb-meia-dobra">
          <h2>A plataforma de e-commerce JN2 é <span>completa e pensada</span> para acompanhar <span>seu negócio de forma customizada</span></h2>
          <p>Não importa se você está começando sua loja agora ou
           se já possui uma, a tecnologia Magento utilizada pela JN2 garante:</p>
        </div>

        <div class="col-12 card-deck">
          <div class="card">
            <div class="card-header">
              <img src="images/icone-beneficios-plataforma.png" alt="Evolução" />
            </div>
            <div class="card-body">
              <h5>Plataforma que evolui com o seu crescimento</h5>
              <p>À medida em que a sua loja cresce, você precisa de mais recursos. A plataforma JN2 permite que você tenha todos os recursos que necessita, apenas quando precisar. Possuímos mais de <span>50 aplicativos para dar suporte à sua loja</span>.</p>
            </div>
          </div>

          <div class="card">
            <div class="card-header">
              <img src="images/icone-beneficios-atualizacao.png" alt="Atualização constante" />
            </div>
            <div class="card-body">
              <h5>Constante atualização da plataforma</h5>
              <p>A plataforma de e-commerce JN2 conta com o <span>desenvolvimento contínuo</span> para novos aplicativos e recursos. Temos uma equipe de profissionais trabalhando constantemente na melhoria dos nossos produtos.</p>
            </div>
          </div>

          <div class="card">
            <div class="card-header">
              <img src="images/icone-beneficios-seguranca.png" alt="Segurança" />
            </div>
            <div class="card-body">
              <h5>Maior segurança para você vender mais</h5>
              <p>Todas as lojas virtuais contam com requisitos de segurança para garantir que todas as <span>transações sejam feitas com confiabilidade</span>, tanto para quem vende quanto para quem compra.</p>
            </div>
          </div>

          <div class="card">
            <div class="card-header">
              <img src="images/icone-beneficios-rapidez.png" alt="Rapidez" />
            </div>
            <div class="card-body">
              <h5>Rapidez na disponibilização da loja</h5>
              <p>Disponibilizamos sua loja virtual em até 10 minutos após o seu cadastro. Depois disso, você já  pode começar a cadastrar seus produtos e editar seu e-commerce.</p>
            </div>
          </div>
        </div>
      </div>

      <div class="row mt-4 depoimentos">
        <div class="col pr-sm-2">
          <div class="media">
            <div class="foto-depoimento">
              <img src="images/foto-julio-cesar.png" alt="Foto Júlio César">
            </div>
            <div class="media-body">
              <img src="images/aspas.png">
              <p>“A loja virtual da JN2 é ágil e muito objetiva nas vendas. A operação é simples, e nos ajudou bastante a escalar nosso negócio on-line."</p>
              <h5>
                Júlio César<br />
                <small>Flyway</small>
              </h5>
            </div>
          </div>
        </div>

        <div class="col pl-sm-2">
          <div class="media">
            <div class="foto-depoimento">
              <img src="images/foto-pedro-haddad.png" alt="Foto Pedro Haddad">
            </div>
            <div class="media-body">
              <img src="images/aspas.png">        
              <p>“A história da Amazônia se divide em antes e depois da JN2. A loja virtual tornou nosso negócio muito mais escalável e multiplicou o alcance da nossa marca."</p>
              <h5>
                Pedro Haddad<br />
                <small>Amâzônia</small>
              </h5>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <!-- Customização -->
  <div class="customizacao bg-purple py-mdb py-sm-db">
    <div class="container">
      <div class="row">
        <div class="col-12">
          <h2>Crie sua <span>Loja Virtual de forma simples, rápida</span>, e ainda customize conforme o seu modelo de negócios.</h2>
          <p class="lead">Você não precisa ter conhecimento técnico para colocar sua loja no ar.</p>

          <div class="row my-mdb">
            <div class="col col-sm-6">
              <div class="media">
                <img class="align-self-start" src="images/icone-customizacao-rapidez.png">
                <div class="media-body">
                  <h3>Rapidez e facilidade para você construir sua Loja Virtual</h3>
                  <p>Monte sua loja de forma rápida e fácil. Você pode escolher um tema e customizar o site. Clique em <a href="#" title="Teste Grátis">Faça um Teste Grátis</a> e comece a criar sua loja agora.</p>
                </div>
              </div>
            </div>

            <div class="col col-sm-6">
              <div class="media">
                <img class="align-self-start" src="images/icone-customizacao-suporte.png">
                <div class="media-body">
                  <h3>Suporte Ilimitado, sempre que for necessário</h3>
                  <p><span>Sem limites de horas de suporte</span> com a JN2. Entre em contato por e-mail ou por telefone sempre que for necessário, e responderemos o mais rápido possível dentro do horário comercial.</p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div class="row justify-content-center depoimentos">
        <div class="col-10 col-sm-8 pl-sm-0 pr-sm-3">
          <div class="media">
            <img src="images/foto-adriana-taissun.png" alt="Foto Adriana Taissun">
            <div class="media-body">
              <img src="images/aspas.png">
              <p>“A JN2 superou todas as minhas expectativas. O escopo do projeto foi 100% cumprido com muita rapidez. O time é super atencioso, e além disso, a loja ficou linda!"</p>
              <h5>
                Adriana Taissun<br />
                <small>Agência Coruja</small>
              </h5>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <!-- Equipe Especialistas -->
  <div class="equipe py-mdb py-sm-db">
    <div class="container">
      <div class="row">
        <div class="col col-sm-8">
          <h2>Você não precisa ter expertise para começar a vender on-line.</h2>
          <p class="lead">Conte com uma plataforma capaz de se adaptar à <span>sua realidade</span>, e com uma <span>equipe de especialistas</span> pronta para te ajudar em todas as etapas do seu projeto.</p>
        </div>
      </div>

      <!-- Conversão -->
      <div class="row conversao">
        <div class="col-12 col-md-9 d-md-flex">
          <div class="d-flex d-md-block justify-content-center">
            <a id="BtnFaleEspecialistasHome" class="btn btn-indigo" href="#" role="button" data-toggle="modal" data-target="#ModalExpert">Fale com um expert</a>
          </div>

          <div class="d-flex d-md-block justify-content-center">
            <p class="ou">OU</p>
          </div>

          <div class="d-md-block justify-content-center">
            <a id="BtnTesteEspecialistasHome" class="btn btn-indigo" href="comece-seu-teste-gratis" role="button">Faça um teste grátis</a>
            <p class="seguranca"><small>Teste por 15 dias, sem compromissos.<br />
            Não precisa de cartão de crédito.</small></p>
          </div>

          <div class="d-flex d-md-block justify-content-center">
            <div class="selo">
              Suporte técnico e
              de uso com horas
              <span class="font-weight-bold">ILIMITADAS</span>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <!-- Recursos -->
  <div class="recursos py-mdb py-sm-db" id="recursos">
    <div class="container">
      <div class="row">
        <div class="col-12">
          <h2 class="pb-mdb">Conheça os principais recursos da Plataforma JN2 e entenda por que já temos centenas de lojas publicadas</h2>
        </div>
      </div>

      <div class="row">
        <div class="col col-sm-4">
          <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
            <a class="nav-link active" id="v-pills-performance-tab" data-toggle="pill" href="#v-pills-performance" role="tab" aria-controls="v-pills-performance" aria-selected="true">
              <h4>Performance de Vendas</h4>
              <p>Sua loja pronta para atender seus clientes e vender mais.</p>
            </a>

            <a class="nav-link" id="v-pills-design-tab" data-toggle="pill" href="#v-pills-design" role="tab" aria-controls="v-pills-design" aria-selected="false">
              <h4>Design</h4>
              <p style="display: none">Crie sua loja do seu jeito de forma simples: garanta alta performance e experiência do usuário.</p>
            </a>

            <a class="nav-link" id="v-pills-catalogo-tab" data-toggle="pill" href="#v-pills-catalogo" role="tab" aria-controls="v-pills-catalogo" aria-selected="false">
              <h4>Catálogo de Produtos</h4>
              <p style="display: none">Exponha seu produto da melhor forma na sua loja.</p>
            </a>

            <a class="nav-link" id="v-pills-gestao-tab" data-toggle="pill" href="#v-pills-gestao" role="tab" aria-controls="v-pills-gestao" aria-selected="false">
              <h4>Gestão de Pedidos</h4>
              <p style="display: none">Tenha total controle de tudo o que você vende.</p>
            </a>

            <a class="nav-link" id="v-pills-marketing-tab" data-toggle="pill" href="#v-pills-marketing" role="tab" aria-controls="v-pills-marketing" aria-selected="false">
              <h4>Ferramentas de Marketing</h4>
              <p style="display: none">Tudo o que você precisa para expor seus produtos na loja.</p>
            </a>

            <a class="nav-link" id="v-pills-relatorios-tab" data-toggle="pill" href="#v-pills-relatorios" role="tab" aria-controls="v-pills-relatorios" aria-selected="false">
              <h4>Relatórios</h4>
              <p style="display: none">Saiba de tudo o que acontece na sua Loja Virtual.</p>
            </a>

            <a class="nav-link" id="v-pills-integracoes-tab" data-toggle="pill" href="#v-pills-integracoes" role="tab" aria-controls="v-pills-integracoes" aria-selected="false">
              <h4>Integrações</h4>
              <p style="display: none">Integre sua loja com tudo o que você precisa para vender e gerenciar.</p>
            </a> 
          </div>
        </div>

        <div class="col col-sm-8">
          <div class="tab-content" id="v-pills-tabContent">
            <div class="tab-pane fade show active" id="v-pills-performance" role="tabpanel" aria-labelledby="v-pills-performance-tab">
              <div class="card-deck">
                <div class="card">
                  <img src="images/recursos-conclusao.png">
                  <div class="card-body">
                    <h5>Facilite a conclusão da venda</h5>
                    <p>Permita que seu cliente finalize a compra de forma rápida e sem burocracias com o <span>Fast Chek-out</span>, funcionalidade da plataforma JN2 pensada para aumentar suas vendas.</p>
                  </div>
                </div>

                <div class="card text-center">
                  <img src="images/recursos-carrinhos.png">
                  <div class="card-body">
                    <h5 class="card-title">Recupere Carrinhos Abandonados</h5>
                    <p class="card-text">Um visitante que inicia a compra e, por algum motivo, não conclui, continua sendo um cliente em potencial. Nós te ajudamos a identificar <span>estes visitantes que abandonam o carrinho</span>.</p>
                  </div>
                </div>
              </div>

              <!--<div class="col-12 text-center">
                <a id="BtnRecursosPerformance" class="btn btn-green btn-lg" href="#" role="button">Ver mais recursos</a>
              </div>-->
            </div>
            
            <div class="tab-pane fade" id="v-pills-design" role="tabpanel" aria-labelledby="v-pills-design-tab">
              <div class="card-deck">
                <div class="card text-center">
                  <img src="images/recursos-estilizacao.png">
                  <div class="card-body">
                    <h5 class="card-title">Escolha um tema gratuito ou contrate um serviço de estilização</h5>
                    <p class="card-text">Deixe o <span>seu site do seu jeito</span> escolhendo um dos temas disponíveis. Todos os temas são otimizados para converter seus visitantes em clientes.</p>
                  </div>
                </div>

                <div class="card text-center">
                  <img src="images/recursos-mobile.png">
                  <div class="card-body">
                    <h5 class="card-title">100% otimizado para Mobile</h5>
                    <p class="card-text">Garanta a <span>melhor experiência para os seus clientes</span> em todos os dispositivos. A Plataforma JN2 é totalmente otimizada para acessos via desktop, tablets e celulares.</p>
                  </div>
                </div>
              </div>

              <!--<div class="col-12 text-center">
                <a id="BtnRecursosDesign" class="btn btn-green btn-lg" href="#" role="button">Ver mais recursos</a>
              </div>-->
            </div>
            
            <div class="tab-pane fade" id="v-pills-catalogo" role="tabpanel" aria-labelledby="v-pills-catalogo-tab">
              <div class="card-deck">
                <div class="card text-center">
                  <img src="images/recursos-atributos.png">
                  <div class="card-body">
                    <h5 class="card-title">Facilite a busca dos seus clientes com filtros e atributos</h5>
                    <p class="card-text">Segmente seus produtos de acordo com as características principais, independente do que sua loja vende. Maior <span>facilidade para seu cliente e melhor controle para você</span>.</p>
                  </div>
                </div>

                <div class="card text-center">
                  <img src="images/recursos-estoque.png">
                  <div class="card-body">
                    <h5 class="card-title">Quer vender produtos físicos? Você precisa controlar seu estoque</h5>
                    <p class="card-text">Com a plataforma JN2, o <span>controle de estoque</span> é feito  de forma automática. Você não tem que se preocupar se está disponibilizando um produto que já esgotou.</p>
                  </div>
                </div>
              </div>

              <!--<div class="col-12 text-center">
                <a id="BtnRecursosCatalogo" class="btn btn-green btn-lg" href="#" role="button">Ver mais recursos</a>
              </div>-->
            </div>

            <div class="tab-pane fade" id="v-pills-gestao" role="tabpanel" aria-labelledby="v-pills-gestao-tab">
              <div class="card-deck">
                <div class="card text-center">
                  <img src="images/recursos-rastreamento.png">
                  <div class="card-body">
                    <h5 class="card-title">Envio de Código de Rastreamento</h5>
                    <p class="card-text">Informe todos os detalhes do pedido para o seu cliente por meio de códigos de rastreamento. Assim, <span>seu cliente fica mais seguro</span> quanto ao recebimento do produto.</p>
                  </div>
                </div>

                <div class="card text-center">
                  <img src="images/recursos-pedidos.png">
                  <div class="card-body">
                    <h5 class="card-title">Cancele pedidos não pagos automaticamente</h5>
                    <p class="card-text">Muitas vezes, a compra não é aprovada pelo emissor do cartão ou pelo banco do cliente. <span>Você não precisa se preocupar caso isso ocorra</span>: a plataforma faz o cancelamento automático.</p>
                  </div>
                </div>

                <!--<div class="col-12 text-center">
                  <a id="BtnRecursosGestao" class="btn btn-green btn-lg" href="#" role="button">Ver mais recursos</a>
                </div>-->
              </div>
            </div>

            <div class="tab-pane fade" id="v-pills-marketing" role="tabpanel" aria-labelledby="v-pills-marketing-tab">
              <div class="card-deck">
                <div class="card text-center">
                  <img src="images/recursos-receita.png">
                  <div class="card-body">
                    <h5 class="card-title">Aumente sua receita com Up-Sell e Cross-Sell</h5> 
                    <p class="card-text"><span>Estimule seus clientes</span> a comprar mais oferecendo produtos similares (de valor mais alto) ou produtos complementares.Informe todos os detalhes do pedido para o seu cliente por meio de códigos de rastreamento. Assim, seu cliente fica mais seguro quanto ao recebimento do produto.</p>
                  </div>
                </div>

                <div class="card text-center">
                  <div class="card-body">
                  <img src="images/recursos-vistos.png">
                    <h5 class="card-title">Itens vistos recentemente</h5>
                    <p class="card-text">Muitas pessoas encontram um produto que gostam, mas não compram no primeiro momento. Permita que seus visitantes tenham <span>fácil acesso aos produtos</span> que gostaram anteriormente e facilite o processo de venda.</p>
                  </div>
                </div>

                <!--<div class="col-12 text-center">
                  <a id="BtnRecursosMarketing" class="btn btn-green btn-lg" href="#" role="button">Ver mais recursos</a>
                </div>-->
              </div>
            </div>

            <div class="tab-pane fade" id="v-pills-relatorios" role="tabpanel" aria-labelledby="v-pills-relatorios-tab">
              <div class="card-deck">
                <div class="card text-center">
                  <img src="images/recursos-vendas.png">
                  <div class="card-body">
                    <h5 class="card-title">Acompanhe as vendas no Dashboard</h5>
                    <p class="card-text">A plataforma já possui um dashboard de vendas, disponível assim que você acessa. <span>Acompanhe de perto todas as vendas</span> e resultados da sua Loja Virtual.</p>
                  </div>
                </div>

                <div class="card text-center">
                  <img src="images/recursos-relatorio.png">
                  <div class="card-body">
                    <h5 class="card-title">Entenda mais do seu negócio com relatórios de produtos mais vistos</h5>
                    <p class="card-text"><span>Saiba exatamente o que as pessoas procuram</span> no seu site e analise as vendas de cada um destes produtos. Além disso, também é possível ver relatórios com o estoque de cada produto.</p>
                  </div>
                </div>

                <!--<div class="col-12 text-center">
                  <a id="BtnRecursosRelatorios" class="btn btn-green btn-lg" href="#" role="button">Ver mais recursos</a>
                </div>-->
              </div>
            </div>

            <div class="tab-pane fade" id="v-pills-integracoes" role="tabpanel" aria-labelledby="v-pills-integracoes-tab">
              <div class="card-deck">
                <div class="card text-center">
                  <img src="images/recursos-erp.png">
                  <div class="card-body">
                    <h5 class="card-title">Integre sua loja com seu ERP</h5>
                    <p class="card-text"><span>Facilite todo o seu processo</span> integrando sua loja com os principais ERP’s do mercado.</p>
                  </div>
                </div>

                <div class="card text-center">
                  <img src="images/recursos-integracao.png">
                  <div class="card-body">
                    <h5 class="card-title">Tudo integrado de ponta a ponta</h5>
                    <p class="card-text"><span>Integre sua loja</span> com os principais meios de pagamentos, serviços logísticos e até mesmo Marketplaces.</p>
                  </div>
                </div>

                <!--<div class="col-12 text-center">
                  <a id="BtnRecursosIntegracoes" class="btn btn-green btn-lg" href="#" role="button">Ver mais recursos</a>
                </div>-->
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <!--Funcionalidades -->
  <div class="funcionalidades pt-mdb py-sm-db">
    <div class="container">
      <div class="row justify-content-end">
        <div class="col-12 pb-mdb">
          <h2><u>Mais de 500 funcionalidades</u> pensadas para aumentar a conversão das suas páginas e você <span>vender mais</span></h2>
        </div>
        
        <div class="col col-lg-4">
          <div class="media">
            <img src="images/icone-checkout.png" alt="" class="align-self-start">
            <div class="media-body">
              <h3>Check-out Simples</h3>
              <p>Solução de check-out em uma única tela.</p>
            </div>
          </div>

          <div class="media">
            <img src="images/icone-pagamentos.png" alt="" class="align-self-start">
            <div class="media-body">
              <h3>Soluções de Pagamento</h3>
              <p>Pagamentos com todas as bandeiras de cartões de crédito, débito, transferência, pagamento na entrega e boleto.</p>
            </div>
          </div>

          <div class="media">
            <img src="images/icone-erp.png" alt="">
            <div class="media-body">
              <h3>Integração com ERPs</h3>
              <p>Otimize a gestão da sua loja integrando os dados do seu ERP com a sua Loja Virtual.</p>
            </div> 
          </div>

          <div class="media">
            <img src="images/icone-marketplace.png" alt="" class="align-self-start">
            <div class="media-body">
              <h3>Integração com Marketplaces</h3>
              <p>Venda nos principais Marketplaces (plataformas, como Mercado Livre e Extra, que comercializam produtos de diversas lojas).</p>
            </div>
          </div>
        </div>

        <div class="col col-lg-4">
          <div class="media">
            <img src="images/icone-antifraude.png" alt="" class="align-self-start">
            <div class="media-body">
              <h3>Sistemas AntiFraude</h3>
              <p>Garanta maior segurança para todas as suas transações utilizando nosso sistema antifraudes.</p>
            </div>
          </div>

          <div class="media">
            <img src="images/icone-emkt.png" alt="" class="align-self-start">
            <div class="media-body">
              <h3>E-mail Marketing</h3>
              <p>Facilite o envio de e-mails para seus clientes integrando sua loja à plataforma de e-mail marketing.</p>
            </div>
          </div>

          <div class="media">
            <img src="images/icone-seo.png" alt="" class="align-self-start">
            <div class="media-body">
              <h3>SEO</h3>
              <p>Recursos para otimização dos resultados em mecanismos de busca, como o Google.</p>
            </div>
          </div>

          <div class="media">
            <img src="images/icone-foguete.png" alt="" class="align-self-start">
            <div class="media-body">
              <h3 class="text-green">E muitas outras…</h3>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <!-- Infraestrutura -->
  <div class="infraestrutura py-mdb py-sm-db">
    <div class="container">
      <div class="row">
        <div class="col-12 mb-sm-mdb">
          <h2>Deixe todas as <span>questões técnicas por nossa conta</span>, e tenha maior tranquilidade para você focar no crescimento do seu negócio</h2>
          <p class="lead">Datacenter, servidores, loadbalance, CDN e todos os demais aspectos da hospedagem, <span>tudo incluso no valor da mensalidade</span>.</p>
        </div>

        <div class="col-12 col-sm-6 my-mdb my-sm-0">
          

          <div class="media">
            <img src="images/icone-questoes-tecnicas.jpg" alt="" class="align-self-start">
            <div class="media-body">
              <p>Ambiente seguro e com alta disponibilidade: seu site sempre no ar e funcionando.</p>
            </div>
          </div>

          <div class="media">
            <img src="images/icone-questoes-tecnicas.jpg" alt="" class="align-self-start">
            <div class="media-body">
              <p>Sua loja hospedada na Amazon AWS, um dos maiores ambientes do mundo.</p>
            </div>
          </div>
        </div>

        <div class="col-12 col-sm-6">
          <img src="images/servidor.jpg" alt="" class="img-fluid">
        </div> 
      </div>
    </div>
  </div>

  <!-- Comissões -->
  <div class="comissoes py-mdb py-sm-db">
    <div class="container">
      <div class="row">
        <div class="col col-sm-8 offset-sm-4">
          <h2>Aumente sua receita vendendo on-line com uma plataforma que facilita o seu trabalho e não cobra comissões sobre vendas.</h2>
        </div>
      </div>

      <!-- Conversão -->
      <div class="row conversao">
        <div class="col-12 col-md-9 offset-sm-2 d-md-flex">
          <div class="d-flex d-md-block justify-content-center">
            <div class="selo">
              Suporte técnico e de uso com horas
              <span class="font-weight-bold">ILIMITADAS</span>
            </div>
          </div>

          <div class="d-flex d-md-block justify-content-center">
            <a id="BtnFaleComissoesHome" class="btn btn-indigo" href="#" role="button" data-toggle="modal" data-target="#ModalExpert">Fale com um expert</a>
          </div>

          <div class="d-flex d-md-block justify-content-center">
            <p class="ou">OU</p>
          </div>

          <div class="d-md-block justify-content-center">
            <a id="BtnTesteComissoesHome" class="btn btn-indigo" href="comece-seu-teste-gratis" role="button">Faça um teste grátis</a>
            <p class="seguranca"><small>Teste por 15 dias, sem compromissos.<br />
            Não precisa de cartão de crédito.</small></p>
          </div>
        </div>
      </div>
    </div>
  </div>

  <!-- Planos -->
  <div id="planos" class="planos py-mdb py-sm-db">
    <div class="container">
      <div class="row justify-content-center">
        <div class="col-10 mb-mdb">
          <h2>Escolha o plano que mais combina com a sua empresa, sem fidelização. <span>Você pode alterar no futuro</span>, conforme sua necessidade.</h2>
          <p class="lead">SEM set-up e Comissão ZERO em todos os planos. Tudo o que você vende fica com você.</p>
        </div>
        
        <div class="col-10 col-sm-12">
          <div class="card-deck tipos-planos">
            <!-- Tropos -->
            <div class="card">
              <div class="card-header">
                Tropos
              </div>
              <div class="card-body">
                <p>Para pequenas Lojas Virtuais</p>
                <p class="preco">R$ 399 <small>mensais</small></p>
              </div>
              <div class="card-footer">
                <a id="BtnContratarTroposPlanosHome" class="btn btn-orange" href="https://lojadeplanos.jn2.com.br/home/planos/tropos-mensal.html" target="_target" role="button">Contratar</a>                
                <dl class="row">
                  <dt class="col-8 col-sm-8">Qde. Produtos</dt>
                  <dd class="col-4 col-sm-4">500</dd>
                  <dt class="col-8 col-sm-8">Pageviews</dt>
                  <dd class="col-4 col-sm-4">40.000</dd>
                  <dt class="col-8 col-sm-8">Armazenamento</dt>
                  <dd class="col-4 col-sm-4">600 MB</dd>
                </dl>
              </div>
            </div>
            
            <!-- Mesos -->
            <div class="card">
              <div class="card-header">
                Mesos
              </div>
              <div class="card-body">
                <p>Para pequenas Lojas Virtuais</p>
                <p class="preco">R$ 549 <small>mensais</small></p>
                
              </div>
              <div class="card-footer">
                <a id="BtnContratarMesosPlanosHome" class="btn btn-orange" href="https://lojadeplanos.jn2.com.br/home/planos/mesos-mensal.html" target="_target" role="button">Contratar</a>
                <dl class="row">
                  <dt class="col-8 col-sm-8">Qde. Produtos</dt>
                  <dd class="col-4 col-sm-4">750</dd>
                  <dt class="col-8 col-sm-8">Pageviews</dt>
                  <dd class="col-4 col-sm-4">60.000</dd>
                  <dt class="col-8 col-sm-8">Armazenamento</dt>
                  <dd class="col-4 col-sm-4">1 GB</dd>
                </dl>
              </div>
            </div>
            
            <!-- Termos -->
            <div class="card">
              <div class="card-header">
                Termos
              </div>
              <div class="card-body">
                <p>Para quem já tem muitos produtos</p>
                <p class="preco">R$ 749 <small>mensais</small></p>
              </div>
              <div class="card-footer">
                <a id="BtnContratarTermosPlanosHome" class="btn btn-orange" href="https://lojadeplanos.jn2.com.br/home/planos/termos-mensal.html" target="_target" role="button">Contratar</a>
                <dl class="row">
                  <dt class="col-8 col-sm-8">Qde. Produtos</dt>
                  <dd class="col-4 col-sm-4">1.000</dd>
                  <dt class="col-8 col-sm-8">Pageviews</dt>
                  <dd class="col-4 col-sm-4 text-nowrap">80.000</dd>
                  <dt class="col-8 col-sm-8">Armazenamento</dt>
                  <dd class="col-4 col-sm-4 text-nowrap">1 GB</dd>
                </dl>
              </div>
            </div>

            <!-- Ionos -->
            <div class="card">
              <div class="card-header">
                Ionos
              </div>
              <div class="card-body">
                <p>Para lojas em rápido crescimento</p>
                <p class="preco">R$ 949 <small>mensais</small></p>
              </div>
              <div class="card-footer">
                <a id="BtnContratarIonosPlanosHome" class="btn btn-orange" href="https://lojadeplanos.jn2.com.br/home/planos/ionos-mensal.html" target="_target" role="button">Contratar</a>
                <dl class="row">
                  <dt class="col-8 col-sm-8">Qde. Produtos</dt>
                  <dd class="col-4 col-sm-4">2.000</dd>
                  <dt class="col-8 col-sm-8">Pageviews</dt>
                  <dd class="col-4 col-sm-4 text-nowrap">120.000</dd>
                  <dt class="col-8 col-sm-8">Armazenamento</dt>
                  <dd class="col-4 col-sm-4 text-nowrap">2 GB</dd>
                </dl>
              </div>
            </div>
          </div>

          <div class="col-12 separador-planos">
              <div class="foguete">
                <img src="images/foguete.jpg" alt="foguete" />
              </div>

              <hr />
          </div>

          <div class="card-deck recursos-planos" style="display: none;">
            <div class="card bg-light">
              <div class="card-body">
                <p>Integração com meios de pagamento</p>
                <p>Design Responsivo</p>
                <p>Integração com ERPs parceiros</p>
                <p>Integração com Correios</p>
                <p>Integração com antifraudes ClearSale e Konduto</p>
                <p>Certificado de segurança SSL</p>
                <p>API Aberta</p>
                <p>Check-out em uma página</p>
                <p>UpSelling & CrossSelling</p>
              </div>
              
              <div class="card-footer">
                <a id="BtnListaTroposPlanosHome" class="btn btn-outline-green" href="https://lojadeplanos.jn2.com.br/home/planos/tropos-mensal.html" target="_target" role="button">Ver lista completa</a>
              </div>
            </div>          

            <div class="card bg-light">
              <div class="card-body">
                <p><span class="nome-plano">Plano Tropos</span><br />
                <span class="numero-funcionalidades">+10 Funcionalidades</span></p>

                <p>Recuperação de carrinhos abandonados</p>
                <p>Integração com:</p>
                <ul>
                  <li>Gateway Logístico Frenet</li>
                  <li>Marketplaces by Plugg.to</li>
                  <li>Google Tag Manager</li>
                  <li>MailChimp</li>
                </ul>
                <p>Níveis de Desconto por quantidade de produto.</p>
                <p>Etiquetas Promocionais para foto de produtos</p>
                <p>Grupos de clientes com política de preços flexível e configurável</p>
                <p>Automatização de liberação de estoque em caso de não pagamento</p>
              </div>

              <div class="card-footer">
                <a id="BtnListaMesosPlanosHome" class="btn btn-outline-green" href="https://lojadeplanos.jn2.com.br/home/planos/mesos-mensal.html" target="_target" role="button">Ver lista completa</a>
              </div>
            </div>

            <div class="card bg-light">
              <div class="card-body">
                <p><span class="nome-plano">Plano Mesos</span><br />
                <span class="numero-funcionalidades">+6 Funcionalidades</span></p>

                <p>Blog</p>
                <p>Ajax Cart Pró</p>
                <p>Navegação Avançada</p>
                <p>Busca com Autocomplete</p>
                <p>Importação e exportação de catálogo e lista de clientes</p>
                <p>Customização de e-mails transacionais</p>
              </div>

              <div class="card-footer">
                <a id="BtnListaTermosPlanosHome" class="btn btn-outline-green" href="https://lojadeplanos.jn2.com.br/home/planos/termos-mensal.html" target="_target" role="button">Ver lista completa</a>
              </div>
            </div>

            <div class="card bg-light">
              <div class="card-body">
                <p><span class="nome-plano">Plano Termos</span><br />
                <span class="numero-funcionalidades">+4 Funcionalidades</span></p>

                <p>Produtos Relacionados Automaticamente</p>
                <p>Relatórios Avançados</p>
                <p>Troca e Devolução</p>
                <p>Programa de Fidelidade</p>
              </div>

              <div class="card-footer">
                <a id="BtnListaIonosPlanosHome" class="btn btn-outline-green" href="https://lojadeplanos.jn2.com.br/home/planos/ionos-mensal.html" target="_target" role="button">Ver lista completa</a>
              </div>
            </div>
          </div>

          <div class="col-12">
            <a id="BtnMaisPlanosHome" class="btn btn-indigo btn-lg btn-vermais" role="button">Ver mais <i class="fas fa-angle-down"></i></a>
          </div>
        </div>

        <div class="col-10 frase-convencimento">
          <h3>Clique em <a id="BtnFalePlanosHome" href="#" role="button" data-toggle="modal" data-target="#ModalExpert">Fale com um expert</a> para saber sobre opções de planos com mais produtos, pageviews, armazenamento ou sobre os mais de 50 aplicativos extras e serviços opcionais de consultoria de implantação e estilização de tema.</h3>
        </div>
      </div>
    </div>
  </div>

  <!-- Razões para contratar -->
  <div class="razoes py-mdb py-sm-db">
    <div class="container">
      <div class="row justify-content-center">
        <div class="col col-md-7">
          <h2 class="pb-mdb">5 razões para você começar seu e-commerce com a JN2:</h2>
          <ul class="list-unstyled">
            <li>
              <div class="align-self-center numero">1</div> 
              <span>Não cobramos comissões</span> sobre suas vendas. Tudo o que você vender fica com você.
            </li>
            <li>
              <div class="align-self-center numero">2</div> 
              Infraestrutura robusta para garantir <span>segurança às suas transações e aos seus dados</span>.
            </li>
            <li>
              <div class="align-self-center numero">3</div> 
              <span>Plataforma flexível</span> com integrações com os principais players do mercado.
            </li>
            <li>
              <div class="align-self-center numero">4</div> 
              <span>Disponibilização rápida e sem complicações</span> para você começar a criar sua loja ainda hoje.
            </li>
            <li>
              <div class="align-self-center numero">5</div> 
              Plataforma que cresce com o seu negócio. <span>Atualizações contínuas</span> para garantir o que há de mais moderno.
            </li>
          </ul>
        </div>
      </div>
    </div>
  </div>

  <!-- Dúvidas -->
  <div class="duvidas py-mdb py-sm-db">
    <div class="container">
      <div class="row justify-content-center">
        <div class="col col-md-10 justify-content-center">
          <h2 class="mb-mdb">Respondemos algumas perguntas mais comuns sobre as nossas lojas virtuais</h2>
        </div>
      </div>

      <div class="row justify-content-center">
        <div class="col col-md-8">
          <div class="accordion" id="accordionExample">
            <div class="card duvida">
              <div class="card-header" id="headingOne">
                <h5 class="m-0">
                  <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#duvidaUm" aria-expanded="false" aria-controls="duvidaUm">
                    Em quanto tempo minha loja estará pronta para vender?
                  </button>
                </h5>
              </div>
              <div id="duvidaUm" class="collapse" aria-labelledby="headingOne" data-parent="#accordionExample">
                <div class="card-body">
                  Em até três dias úteis você receberá a loja em um endereço provisório e o acesso ao painel administrativo. Então inicia-se a implantação, que depende basicamente de seu empenho, que é o que definirá o tempo que levará até a publicação. Você deverá se familiarizar com a plataforma, cadastrar seus produtos e páginas internas, configurar os métodos de pagamento, entrega e demais módulos. Depois disso, sua loja será publicada por nossa equipe. Este processo demora, em média, 30 dias.
                </div>
              </div>
            </div>

            <div class="card duvida">
              <div class="card-header" id="headingTwo">
                <h5 class="m-0">
                  <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#duvidaDois" aria-expanded="false" aria-controls="duvidaDois">
                    Quem vai realizar as configurações, cadastro de produtos e integrações da minha loja?
                  </button>
                </h5>
              </div>
              <div id="duvidaDois" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
                <div class="card-body">
                  A inserção de todo conteúdo da loja, bem como as configurações, cadastro de produtos, banners e chaves de integração são feitas pelos nossos clientes. O cliente também pode optar por contratar a consultoria de implantação, que o ajudará neste processo, dando instruções e dicas de operação da plataforma.
                </div>
              </div>
            </div>

            <div class="card duvida">
              <div class="card-header" id="headingThree">
                <h5 class="m-0">
                  <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#duvidaTres" aria-expanded="false" aria-controls="duvidaTres">
                    Loja tema, estilização de tema ou criação de novo tema? É possível personalizar o tema da minha loja ou contratar alguém para desenvolvê-lo para mim?
                  </button>
                </h5>
              </div>
              <div id="duvidaTres" class="collapse" aria-labelledby="headingThree" data-parent="#accordionExample">
                <div class="card-body">
                  As lojas virtuais desenvolvidas pela JN2 contam com design focado em conversão e têm excelente experiência de navegação. O cliente tem três opções ao contratar a JN2. A primeira é escolher um dos temas profissionais em nosso catálogo, neste caso, a loja é entregue com as cores e disposição de blocos do tema escolhido.
                  
                  Se deseja um layout personalizado, poderá contratar a estilização de tema. Com ela é possível mudar as cores dos elementos gráficos, adequar tipografia, espaçamentos dos tamanhos das fontes e da iconografia  a partir de um dos temas disponibilizadas pela JN2. Além disso, o cliente também tem direito à personalização de um banner.
                  
                  Para clientes que têm alguma necessidade específica quanto ao layout, é indicada a contratação de um novo tema, de acordo com as especificidades.
                  
                  A JN2 não replica temas de terceiros, por isso, fazendo esta opção o cliente terá hospedá-las externamente e trabalhar com redirecionamentos entre a loja e elas.
                </div>
              </div>
            </div>

            <div class="card duvida">
              <div class="card-header" id="headingFour">
                <h5 class="m-0">
                  <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#duvidaQuatro" aria-expanded="false" aria-controls="duvidaQuatro">
                    A JN2, além de hospedar minha loja, também hospedará meus e-mails?
                  </button>
                </h5>
              </div>
              <div id="duvidaQuatro" class="collapse" aria-labelledby="headingThree" data-parent="#accordionExample">
                <div class="card-body">
                  A JN2 faz a hospedagem apenas da loja virtual. Para criar e-mails corporativos com @seudominio.com.br você deve contratar uma empresa especializada em hospedagem de e-mails. Se não conhecer nenhuma, peça indicação ao nosso time. É importante que isso seja definido ainda na fase pré-publicação, pois é essencial para o funcionamento dos e-mails transacionais.
                </div>
              </div>
            </div>

            <div class="card duvida">
              <div class="card-header" id="headingFive">
                <h5 class="m-0">
                  <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#duvidaCinco" aria-expanded="false" aria-controls="duvidaCinco">
                    Já tenho loja virtual em outra plataforma e quero migrar meus dados. É possível?
                  </button>
                </h5>
              </div>

              <div id="duvidaCinco" class="collapse" aria-labelledby="headingThree" data-parent="#accordionExample">
                <div class="card-body">
                  É possível migrar catálogo de produtos (título, descrição, descrição resumida, preço, URL, imagem), categorias, clientes (com campos obrigatórios, como CPF, cadastrados), pedidos (Informações de meios de pagamento anteriores à migração poderão não aparecer.), cupons e páginas. Porém, é preciso analisar diversos fatores, como o fornecedor e o nível de acesso ao servidor da sua loja anterior. Para saber mais, solicite o contato de um de nossos experts em e-commerce para lhe auxiliar na contratação desse serviço.
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <!-- Magento -->
  <div class="magento py-mdb py-sm-db">
    <div class="container">
      <div class="row">
        <div class="col col-sm-8 mb-sm-4 pl-sm-0">
          <h2 class="mb-4">Plataforma desenvolvida a partir do Magento Open Source</h2>
          <p class="lead">Usando o Magento, nossa plataforma permite que você crie lojas mais flexíveis, seguras e estáveis. Somos uma das poucas Solution Partner do Magento no Brasil.</p>
        </div>

        <div class="d-none d-sm-block col-sm-4 justify-content-center">
          <img src="images/marca-magento.png" class="mx-auto" alt="" />
        </div>
      </div>

      <!-- Conversão -->
      <div class="row conversao">
        <div class="col-12 col-md-9 d-md-flex">
          <div class="d-flex d-md-block justify-content-center">
            <a id="BtnFaleMagentoHome" class="btn btn-indigo" href="#" role="button" data-toggle="modal" data-target="#ModalExpert">Fale com um expert</a>
          </div>

          <div class="d-flex d-md-block justify-content-center">
            <p class="ou">OU</p>
          </div>

          <div class="d-md-block justify-content-center">
            <a id="BtnTesteMagentoHome" class="btn btn-indigo" href="comece-seu-teste-gratis" role="button">Faça um teste grátis</a>
            <p class="seguranca"><small>Teste por 15 dias, sem compromissos.<br />
            Não precisa de cartão de crédito.</small></p>
          </div>
        </div>
      </div>
    </div>
  </div>

  <?php require_once("footer-home.php"); ?>