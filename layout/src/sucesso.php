<?php require_once("header-inner.php"); ?>

  <div class="cointainer">
    <div class="row justify-content-center my-dobra mx-0">
      <div class="col col-sm-6 mensagem-sucesso">
        <h2>Cadastro concluído com sucesso</h2>

        <h3>Acesse seu e-mail e confirme a criação de sua loja virtual</h3>

        <p>Caso o e-mail não esteja em sua caixa de entrada, verifique sua caixa de spam.</p>

        <a href="https://www.jn2.com.br" class="btn btn-orange">Voltar para o site</a>
      </div>
    </div>
  </div>

<?php require_once("footer-inner.php"); ?>