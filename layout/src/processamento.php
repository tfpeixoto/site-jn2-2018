<?php
session_start();

function request($params)
{
    $api = 'https://dashboard.jn2.store/api/v1/';
    $endpoint = 'trials';

    $dados = http_build_query([
        'trial' => [
            'nome' => $params['nome'],
            'email' => $params['email'],
            'telefone' => $params['telefone'],
            'empresa' => $params['empresa'],
            'tema' => $params['tema']
        ]
    ]);

    $headers = [
        'Content-type' => 'application/x-www-form-urlencoded',
        'Content-Length' => strlen($dados),
        'X-User-Email' => 'fernando@jn2.com.br',
        'X-User-Token' => 'WVk54sXz_WWqDKRj9bXV'
    ];

    $params = array('http' => array(
        'method'  => 'POST',
        'content' => $dados,
        'header'       => array_map(function ($h, $v) {return "$h: $v";}, array_keys($headers), $headers)
    ));

    $ctx = stream_context_create($params);
    $resposta = (array)json_decode(file_get_contents($api . $endpoint, null, $ctx));
    return $resposta;
}

function validateRecapcha($token, $action = 'criar_loja')
{
    $secret = '6LcsCHgUAAAAAJywbrkOlRa-LsoSyq9kO1ttdnob';
    $verifyResponse = file_get_contents('https://www.google.com/recaptcha/api/siteverify?secret='.$secret.'&response='.$token);
    $responseData = json_decode($verifyResponse);

    return (bool)$responseData->success;
}

$token = $_POST['token'];
if(validateRecapcha($token)) {
    $params = array_merge($_SESSION, $_POST);

    if(!empty($params['nome']) && !empty($params['email']) && !empty($params['telefone'])
        && !empty($params['empresa']) && !empty($params['tema'])) {

        $resposta = request($params);
        $tentativas = 1;

        while( empty($resposta['id']) && $tentativas < 5 ) {
            $resposta = request($params);
            $tentativas++;
        }

        if(! empty($resposta['id']) ) {
            $resp['success'] = $resposta;
        }
        else {
            $resp['error'] = true;
        }
    }
    else {
        $resp['error'] = true;
    }
}
else {
    $resp['error'] = true;
}

if(!empty($_SESSION)) session_destroy();
echo json_encode($resp);