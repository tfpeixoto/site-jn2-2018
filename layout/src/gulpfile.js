var gulp = require('gulp');
var sass = require('gulp-sass');

gulp.task('sass', function() {
    return gulp.src(['scss/bootstrap4/scss/*.scss', 'scss/*.scss'])
    .pipe(sass()) // converter o Sass em CSS
    .pipe(gulp.dest('css'));
});

gulp.task('watch', function() {
    gulp.watch(['scss/bootstrap4/scss/*.scss', 'scss/*.scss'], ['sass']);
});

gulp.task('default', ['sass', 'watch']);