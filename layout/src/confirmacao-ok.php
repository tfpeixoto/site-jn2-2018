<?php require_once("header-inner.php"); ?>

  <div class="cointainer">
    <div class="row justify-content-center my-dobra">
      <div class="col col-sm-6 mensagem-sucesso">
        <h2>Seu e-mail foi validado com sucesso!</h2>

        <p>Em breve você receberá um e-mail com os dados para acesso a sua loja virtual.</p>

        <a href="https://www.jn2.com.br" class="btn btn-orange">Voltar para o site</a>
      </div>
    </div>
  </div>

<?php require_once("footer-inner.php"); ?>